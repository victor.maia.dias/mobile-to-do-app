# FC Solutions To-Do App

O time deve desenvolver um clássico aplicativo "To-Do" onde um usuário pode anotar todas as tarefas que deseja realizar.

## User Stories

 - O usuário pode ver um campo de entrada onde pode digitar um item de tarefa
 - Ao pressionar enter (ou um botão), o usuário pode enviar o item de tarefa e ver que ele está sendo adicionado a uma lista de tarefas
 - O usuário pode marcar uma tarefa como concluída
 - O usuário pode remover um item de tarefa pressionando um botão (ou no próprio item de tarefa)
 - O usuário pode editar uma tarefa
 - O usuário pode ver uma lista com todas as tarefas concluídas
 - O usuário pode ver uma lista com todas as tarefas ativas
 - O usuario deve poder escanear um QRCode para extrair uma lista de tarefas (o QrCode deve conter texto separados por ";")

