import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'dart:io';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'To-Do List',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHome(),
    );
  }
}

class MyHome extends StatefulWidget {
  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  final TextEditingController taskController = TextEditingController();
  final TextEditingController editController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  List<Map<String, dynamic>> _tasks = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('To-Do List'),
        actions: [
          IconButton(
            icon: Icon(Icons.camera_alt),
            onPressed: _scanQR,
          ),
          IconButton(
            icon: Icon(Icons.list),
            onPressed: () => _filterTasks(false),
          ),
          IconButton(
            icon: Icon(Icons.done_all),
            onPressed: () => _filterTasks(true),
          ),
        ],
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 20),
              child: Form(
                key: _formKey,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: taskController,
                        style: TextStyle(fontSize: 24, color: Colors.black87),
                        decoration: InputDecoration(
                          labelText: 'Nova Tarefa',
                          hintText: 'Digite sua tarefa aqui...',
                          hintStyle: TextStyle(fontSize: 18),
                        ),
                        keyboardType: TextInputType.text,
                        validator: (value) {
                          if (value?.trim().isEmpty ?? true) {
                            return 'Por favor, insira uma tarefa';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20),
                      child: ElevatedButton(
                        child: Text('Adicionar'),
                        onPressed: () {
                          if (_formKey.currentState?.validate() ?? false) {
                            setState(() {
                              _tasks.add({
                                'title': taskController.text.trim(),
                                'completed': false,
                              });
                              taskController.clear();
                            });
                          }
                        },
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.green,
                          foregroundColor: Colors.white,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: _tasks.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      title: Text(
                        _tasks[index]['title'],
                        style: TextStyle(
                          fontSize: 18,
                          decoration: _tasks[index]['completed'] ? TextDecoration.lineThrough : null,
                        ),
                      ),
                      leading: IconButton(
                        icon: Icon(_tasks[index]['completed'] ? Icons.check_circle : Icons.check_circle_outline),
                        onPressed: () {
                          setState(() {
                            _tasks[index]['completed'] = !_tasks[index]['completed'];
                          });
                        },
                        color: _tasks[index]['completed'] ? Colors.green : null,
                      ),
                      trailing: IconButton(
                        icon: Icon(Icons.delete),
                        onPressed: () {
                          setState(() {
                            _tasks.removeAt(index);
                          });
                        },
                      ),
                      onTap: () => _editTask(index),
                    ),
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  void _editTask(int index) {
    editController.text = _tasks[index]['title'];
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('Edite sua tarefa'),
          content: TextFormField(
            controller: editController,
            autofocus: true,
            decoration: InputDecoration(
              hintText: 'Edite sua tarefa',
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Cancelar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('Editar'),
              onPressed: () {
                if (editController.text.isNotEmpty) {
                  setState(() {
                    _tasks[index]['title'] = editController.text.trim();
                    Navigator.of(context).pop();
                  });
                }
              },
            ),
          ],
        );
      },
    );
  }

  void _filterTasks(bool completed) {
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text(completed ? 'Tarefas Concluídas' : 'Tarefas Ativas'),
          content: Container(
            width: double.maxFinite,
            child: ListView(
              children: _tasks.where((task) => task['completed'] == completed).map((task) => ListTile(
                title: Text(task['title']),
              )).toList(),
            ),
          ),
        );
      },
    );
  }

  void _scanQR() async {
    var result = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => QRScanPage()),
    );
    if (result != null) {
      List<String> tasks = result.split(';');
      setState(() {
        for (var task in tasks) {
          if (task.isNotEmpty) {
            _tasks.add({
              'title': task.trim(),
              'completed': false,
            });
          }
        }
      });
    }
  }
}

class QRScanPage extends StatefulWidget {
  @override
  _QRScanPageState createState() => _QRScanPageState();
}

class _QRScanPageState extends State<QRScanPage> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      controller!.pauseCamera();
    } else if (Platform.isIOS) {
      controller!.resumeCamera();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Scanner de QR Code'),
      ),
      body: Stack(
        children: [
          QRView(
            key: qrKey,
            onQRViewCreated: _onQRViewCreated,
            overlay: QrScannerOverlayShape(
              borderColor: Colors.red,
              borderRadius: 10,
              borderLength: 30,
              borderWidth: 10,
              cutOutSize: MediaQuery.of(context).size.width * 0.8,
            ),
          ),
          Positioned(
            bottom: 16.0,
            left: 0,
            right: 0,
            child: Center(
              child: ElevatedButton(
                onPressed: () {
                  Navigator.pop(context); // Fecha a tela de scanner
                },
                child: Text('Fechar Scanner'),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      controller.pauseCamera();
      Navigator.pop(context, scanData.code);  // Passa o resultado do scan para a tela anterior
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }
}
